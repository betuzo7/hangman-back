package com.bnsf.devops.hangman.model.entity;

public enum MaxTime {
    NONE(0), SECS5(5), SECS10(10), SECS20(20), SECS30(30);

    private int seconds;
    MaxTime(int seconds){
        this.seconds=seconds;
    }
    public int getSeconds(){
        return this.seconds;
    }
}
