package com.bnsf.devops.hangman.model.repository;

import com.bnsf.devops.hangman.model.entity.GameAttempt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameAttemptRepository extends JpaRepository<GameAttempt, Integer> {
}
