package com.bnsf.devops.hangman.model.repository;

import com.bnsf.devops.hangman.model.entity.Game;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameRepository extends JpaRepository<Game, Integer> {
}
