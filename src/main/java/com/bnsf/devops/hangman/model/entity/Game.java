package com.bnsf.devops.hangman.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "GAME")
public class Game {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "WORD_HIDDEN")
    private String wordHidden;
    @Enumerated(EnumType.STRING)
    @Column(name = "MAX_TIME")
    private MaxTime maxTime;
    @OneToMany(mappedBy = "game", fetch = FetchType.EAGER)
    private List<GameAttempt> attempts;
}
