package com.bnsf.devops.hangman.config.infrastructure;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import com.bnsf.devops.hangman.ApiController;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableSwagger2
@ComponentScan(basePackages = { "com.bnsf.devops.hangman.controller" })
public class SwaggerConfig {

	@Bean
	public Docket publicServices() {
		final Docket docket = new Docket(DocumentationType.SWAGGER_2).groupName("public").apiInfo(apiInfo()).select()
				.apis(RequestHandlerSelectors.any()).paths(PathSelectors.regex(ApiController.API_PATH_PUBLIC + "/.*")).build()
				.useDefaultResponseMessages(false);
		return docket;
	}

	@Bean
	public Docket privateServices() {
		final List<Parameter> list = new ArrayList<Parameter>();
		list.add(new ParameterBuilder().name("X-Auth-Token").description("Header para tokenId")
				.modelRef(new ModelRef("string")).parameterType("header").required(true).build());
		final Docket docket = new Docket(DocumentationType.SWAGGER_2).groupName("private").apiInfo(apiInfo()).select()
				.apis(RequestHandlerSelectors.any()).paths(PathSelectors.regex(ApiController.API_PATH_PRIVATE + "/.*")).build()
				.globalOperationParameters(list);
		return docket;
	}

	private ApiInfo apiInfo() {
		final String apiTitle = "Overview";
		final String apiDescription = "Game Hangman";
		final String apiVersion = "0.1";
		final String terminsOfService = "termins of service url";
		final Contact contactInformation = new Contact("Hangman", "https://wwww.bnsf.com", "contact@bnsf.com");
		final String license = "License";
		final String licenseUrl = "https://bnsf.com/license";
		return new ApiInfo(apiTitle, apiDescription, apiVersion, terminsOfService, contactInformation, license,
				licenseUrl);
	}

}
