package com.bnsf.devops.hangman.service;

import com.bnsf.devops.hangman.model.entity.Game;
import com.bnsf.devops.hangman.model.entity.GameAttempt;
import com.bnsf.devops.hangman.model.repository.GameAttemptRepository;
import com.bnsf.devops.hangman.model.repository.GameRepository;
import com.bnsf.devops.hangman.service.dto.GameDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.bnsf.devops.hangman.service.dto.StatusGame.*;

@Service
public class DefaultGameService implements GameService{

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private GameAttemptRepository gameAttemptRepository;

    private final static int MAX_ATTEMPTS = 6;

    @Override
    public GameDTO saveGame(Game game) {
        return from(this.gameRepository.save(game));
    }

    @Override
    public List<GameDTO> getGames() {
        return this.gameRepository.findAll().stream().map(this::from).collect(Collectors.toList());
    }

    @Override
    public GameDTO getGameById(Integer id) {
        return from(this.gameRepository.getOne(id));
    }

    @Override
    public GameDTO saveGameAttempt(GameAttempt gameAttempt) {
        Optional<Game> opGame = this.gameRepository.findById(gameAttempt.getGame().getId());
        if (!opGame.isPresent()) {
            return new GameDTO();
        }
        Game game = opGame.get();
        gameAttempt.setOk(isOk(game, gameAttempt.getLetter()));
        GameAttempt attempt = gameAttemptRepository.save(gameAttempt);
        game.getAttempts().add(attempt);
        return from(game);
    }

    private boolean isOk(Game game, Character attempt){
        return game.getWordHidden().contains(new StringBuilder(1).append(attempt));
    }

    private GameDTO from(final Game game){
        final GameDTO dto = new GameDTO();
        dto.setGame(game);
        dto.getGame().setAttempts(dto.getGame().getAttempts() == null ? new ArrayList<>() : dto.getGame().getAttempts());
        dto.setLetters(hiddenWord(game));
        dto.setOks((int) game.getAttempts().stream().filter(GameAttempt::getOk).count());
        dto.setFails(game.getAttempts().size() - dto.getOks());
        dto.setStatusGame((dto.getFails() == MAX_ATTEMPTS) ? LOST : (String.valueOf(dto.getLetters()).contains("X") ? GAMING : WIN));
        return dto;
    }

    private char[] hiddenWord(final Game game){
        char[] letterHidden = new char[game.getWordHidden().length()];
        game.getAttempts().forEach(it -> {
            char[] letters = game.getWordHidden().toCharArray();
            for (int i = 0 ; i < letters.length ; i++){
                if (letters[i] == it.getLetter()) letterHidden[i] = it.getLetter();
            }
        });
        for (int i = 0; i < letterHidden.length; i++) {
            if (0 == letterHidden[i]) letterHidden[i] = 'X';
        }

        return letterHidden;
    }
}
