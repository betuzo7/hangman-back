package com.bnsf.devops.hangman.service.dto;

import com.bnsf.devops.hangman.model.entity.Game;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GameDTO {
    private Game game;
    private char[] letters;
    private int fails;
    private int oks;
    private StatusGame statusGame;
}
