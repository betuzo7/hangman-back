package com.bnsf.devops.hangman.service;

import com.bnsf.devops.hangman.model.entity.Game;
import com.bnsf.devops.hangman.model.entity.GameAttempt;
import com.bnsf.devops.hangman.service.dto.GameDTO;

import java.util.List;

public interface GameService {
    GameDTO saveGame(Game game);

    List<GameDTO> getGames();

    GameDTO getGameById(Integer id);

    GameDTO saveGameAttempt(GameAttempt gameAttempt);
}
