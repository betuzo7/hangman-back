package com.bnsf.devops.hangman.service.dto;

public enum StatusGame {
    GAMING, WIN, LOST
}
