package com.bnsf.devops.hangman.controller.mapper;

import com.bnsf.devops.hangman.controller.dto.GameAttemptRequest;
import com.bnsf.devops.hangman.controller.dto.GameAttemptResponse;
import com.bnsf.devops.hangman.model.entity.Game;
import com.bnsf.devops.hangman.model.entity.GameAttempt;

import java.util.List;
import java.util.stream.Collectors;

public class GameAttemptMapper {
    public static GameAttempt from(GameAttemptRequest gameRequest) {
        GameAttempt gameAttempt = new GameAttempt();
        Game game = new Game();
        game.setId(gameRequest.getId());
        gameAttempt.setGame(game);
        gameAttempt.setLetter(gameRequest.getLetter());
        return gameAttempt;
    }

    public static GameAttemptResponse from(GameAttempt gameAttempt) {
        GameAttemptResponse dto = new GameAttemptResponse();
        dto.setLetter(gameAttempt.getLetter());
        dto.setOk(gameAttempt.getOk());
        return dto;
    }

    public static List<GameAttemptResponse> from(List<GameAttempt> gameAttempts) {
        return gameAttempts.stream().map(GameAttemptMapper::from).collect(Collectors.toList());
    }
}
