package com.bnsf.devops.hangman.controller;

import com.bnsf.devops.hangman.ApiController;
import com.bnsf.devops.hangman.controller.dto.GameAttemptRequest;
import com.bnsf.devops.hangman.controller.dto.GameRequest;
import com.bnsf.devops.hangman.controller.dto.GameResponse;
import com.bnsf.devops.hangman.controller.mapper.GameAttemptMapper;
import com.bnsf.devops.hangman.controller.mapper.GameMapper;
import com.bnsf.devops.hangman.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ApiController.API_PATH_PUBLIC + "/game")
public class GameController {

    @Autowired
    private GameService gameService;

    @ResponseBody
    @PostMapping(value = "")
    public GameResponse createGame(@RequestBody GameRequest gameRequest) {
        return GameMapper.from(this.gameService.saveGame(GameMapper.from(gameRequest)));
    }

    @ResponseBody
    @PostMapping(value = "/{id}/attempt")
    public GameResponse createGameAttempt(@PathVariable Integer id, @RequestBody GameAttemptRequest gameAttemptRequest) {
        return GameMapper.from(this.gameService.saveGameAttempt(GameAttemptMapper.from(gameAttemptRequest)));
    }

    @ResponseBody
    @GetMapping
    public List<GameResponse> readGames() {
        return GameMapper.from(this.gameService.getGames());
    }

    @ResponseBody
    @GetMapping(value = "/{id}")
    public GameResponse readGame(@PathVariable Integer id) {
        return GameMapper.from(this.gameService.getGameById(id));
    }
}
