package com.bnsf.devops.hangman.controller.dto;

import com.bnsf.devops.hangman.model.entity.MaxTime;
import com.bnsf.devops.hangman.service.dto.StatusGame;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class GameResponse {
    private int id;
    private String wordHidden;
    private MaxTime maxTime;
    private StatusGame statusGame;
    private char[] letters;
    private List<GameAttemptResponse> attempts;
    private int fails;
    private int oks;
}
