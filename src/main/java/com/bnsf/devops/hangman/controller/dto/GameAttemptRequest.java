package com.bnsf.devops.hangman.controller.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GameAttemptRequest {
    private int id;
    private char letter;
}
