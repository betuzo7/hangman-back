package com.bnsf.devops.hangman.controller.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GameAttemptResponse {
    private char letter;
    private boolean ok;
}
