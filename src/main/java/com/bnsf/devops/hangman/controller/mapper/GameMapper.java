package com.bnsf.devops.hangman.controller.mapper;

import com.bnsf.devops.hangman.controller.dto.GameRequest;
import com.bnsf.devops.hangman.controller.dto.GameResponse;
import com.bnsf.devops.hangman.model.entity.Game;
import com.bnsf.devops.hangman.service.dto.GameDTO;

import java.util.List;
import java.util.stream.Collectors;

public class GameMapper {
    public static Game from(GameRequest gameRequest) {
        Game game = new Game();
        game.setId(gameRequest.getId() > 0 ? gameRequest.getId() : null);
        game.setWordHidden(gameRequest.getWordHidden().toUpperCase());
        game.setMaxTime(gameRequest.getMaxTime());
        return game;
    }

    public static GameResponse from(GameDTO game) {
        GameResponse gameResponse = new GameResponse();
        gameResponse.setId(game.getGame().getId());
        gameResponse.setWordHidden(game.getGame().getWordHidden());
        gameResponse.setMaxTime(game.getGame().getMaxTime());
        gameResponse.setStatusGame(game.getStatusGame());
        gameResponse.setLetters(game.getLetters());
        gameResponse.setOks(game.getOks());
        gameResponse.setFails(game.getFails());
        gameResponse.setAttempts(GameAttemptMapper.from(game.getGame().getAttempts()));
        return gameResponse;
    }

    public static List<GameResponse> from(List<GameDTO> games) {
        return games.stream().map(GameMapper::from).collect(Collectors.toList());
    }
}
