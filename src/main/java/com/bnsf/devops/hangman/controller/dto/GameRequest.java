package com.bnsf.devops.hangman.controller.dto;

import com.bnsf.devops.hangman.model.entity.MaxTime;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GameRequest {
    private int id;
    private String wordHidden;
    private MaxTime maxTime;
}
