package com.bnsf.devops.hangman.service

import com.bnsf.devops.hangman.model.entity.Game
import com.bnsf.devops.hangman.model.entity.GameAttempt
import spock.lang.Specification
import spock.lang.Unroll

class GameServiceTest extends Specification{

    DefaultGameService gameService = new DefaultGameService()

    def setup(){

    }

    @Unroll("isOk #character in #game.wordHidden")
    def "isOk test"() {
        setup:

        when:
        def ok = gameService.isOk(game, character)

        then:
        ok == result

        where:
        game                               | character   | result
        new Game(wordHidden: "TEST")       | (char)'T'   | true
        new Game(wordHidden: "OTHER")      | (char)'W'   | false
    }

    @Unroll("hiddenWord #result")
    def "hiddenWord test"() {
        setup:

        when:
        def ok = gameService.hiddenWord(game)

        then:
        ok == result

        where:
        game                                               | result
        new Game(wordHidden: "TEST", attempts: [
                new GameAttempt(letter: 'T'),
                new GameAttempt(letter: 'E'),
                new GameAttempt(letter: 'S')])             | ['T', 'E', 'S', 'T']
        new Game(wordHidden: "OTHER", attempts: [])        | ['X','X','X','X','X']
        new Game(wordHidden: "HANGMAN", attempts: [
                new GameAttempt(letter: 'N')])              | ['X','X','N','X','X','X','N']
    }
}
